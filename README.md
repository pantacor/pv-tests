# Pantavisor Tests

## How to Get Started

Go to our [App Engine how to guilde](https://docs.pantahub.com/requirements-appengine/) to know about how to install and run these tests.

## Repository Contents

This repository contains the test data for [Pantavisor Appengine tests](https://gitlab.com/pantacor/pv-scripts/-/tree/master/test):

```shell
common data
```

### common

Contains common resources that are shared between tests.

### data

Contains tests that use [pvr](https://docs.pantahub.com/install-pvr/) and [pvcontrol](https://docs.pantahub.com/local-control/#pvcontrol) to test the [local experience](https://docs.pantahub.com/local-control/).
